package com.company;
// Tiffany (Quinlan) Hofhiens
// create 5 Java Collections

import Week2.MyClass;

import java.awt.desktop.SystemEventListener;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        System.out.println("--- List ---");
        // Creates a new list of strings that will be save and retrieved the order that each item was added.
        List days = new ArrayList();
        days.add("Monday");
        days.add("Tuesday");
        days.add("Wednesday");
        days.add("Thursday");
        days.add("Friday");

        for (Object str : days) {
            System.out.println((String) str);
        }

        System.out.println("--- Set ---");
        // This creates a set of strings and will not allow duplicate items.
        Set fruit = new TreeSet();
        fruit.add("grape");
        fruit.add("banana");
        fruit.add("orange");
        fruit.add("pineapple");
        fruit.add("orange");

        for (Object str : fruit){
            System.out.println((String) str);
        }


        System.out.println("-- Queue --");
        Queue queue = new PriorityQueue();
        //A queue is similar to standing the items in a line. A Priority Queue will order the items alphabetically.
        queue.add("almonds");
        queue.add("cheese");
        queue.add("bacon");
        queue.add("donut");
        queue.add("fish");
        queue.add("egg roll");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        System.out.println("--- Map ---");
        // Maps attach data points together. In this case the number relates to an item. Each number will only have one data point at a time.
        Map menu = new HashMap();
        menu.put(1,"BBQ");
        menu.put(2,"Pizza");
        menu.put(3,"Fish Tacos");
        menu.put(4,"Fajitas");
        menu.put(5,"Curry");
        menu.put(2,"Meatloaf");

        for ( int i =1; i<6; i++){
            String result = (String)menu.get(i);
            System.out.println(result);
        }

        System.out.println("-- List using Generics --");
        List<MyClass> myList = new LinkedList<MyClass>();
        // Will add items to a class. This MyClass is created for book, with a title and author.
        myList.add(new MyClass("The Way of Kings", "Brandon Sanderson"));
        myList.add(new MyClass("Words of Radiance", "Brandon Sanderson"));
        myList.add(new MyClass("Oathbringer", "Brandon Sanderson"));
        myList.add(new MyClass("The Name of the Wind", "Patrick Rothfuss"));

        for (MyClass book : myList) {
            System.out.println(book);
        }

    }
}
