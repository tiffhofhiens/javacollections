package Week2;

import java.util.*;

public class MyClass {

    private String book;
    private String author;

    public MyClass(String book, String author) {
        this.book = book;
        this.author = author;
    }

    public String toString() {
        return "Book Title: " + book + " Author: " + author;
    }

}